CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

SEO Starter provides a jump-start on configuring best practices for SEO.

This module overrides your site's configuration with a set of predefined
configurations influenced by Ben Finklea's
<a href="https://www.drupal8seo.com/">Drupal 8 SEO Book</a>.


REQUIREMENTS
------------

This module requires a suite of SEO-helper modules:

  * coffee (https://www.drupal.org/project/coffee)
  * ctools (https://www.drupal.org/project/ctools)
  * easy_breadcrumb (https://www.drupal.org/project/easy_breadcrumb)
  * google_analytics (https://www.drupal.org/project/google_analytics)
  * hreflang (https://www.drupal.org/project/hreflang)
  * metatag (https://www.drupal.org/project/metatag)
  * metatag_google_plus (https://www.drupal.org/project/metatag)
  * metatag_open_graph (https://www.drupal.org/project/metatag)
  * metatag_twitter_cards (https://www.drupal.org/project/metatag)
  * metatag_verification (https://www.drupal.org/project/metatag)
  * pathauto (https://www.drupal.org/project/pathauto)
  * redirect (https://www.drupal.org/project/redirect)
  * schema_metatag (https://www.drupal.org/project/schema_metatag)
  * schema_web_site (https://schema.org/WebSite)
  * seo_checklist (https://www.drupal.org/project/seo_checklist)
  * simple_sitemap (https://www.drupal.org/project/simple_sitemap)
  * token (https://www.drupal.org/project/token)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information. This module has
   dependencies on a number of different modules that will also be installed.
 * @TODO Figure out how to uninstall.


CONFIGURATION
-------------

This module will replace your website SEO configuration with a pre-defined
configuration provided by this module.

 * In the Drupal administration interface, customize settings for the following
   modules:

    1. Google Analytics
    2. Metatag
    3. Pathauto
    4. Simple Sitemap

* Set permissions: since this module installs other modules, you will need to
  set the permissions for those modules.
* Uninstall: after installation, you can uninstall this module, it does nothing
  after the initial install.
* Manage your configuration: after installation, the settings provided by this
  module and additional customizations you make in the admin UI will be in your
  site's configuration. Manage them as you normally would.


MAINTAINERS
-----------

  * Jim Birch (thejimbirch) - https://www.drupal.org/u/thejimbirch

Supporting Organization:
  * Kanopi Studios - https://www.drupal.org/kanopi-studios
